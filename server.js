"use strict";

// heroku features:enable http-session-affinity


var path = require('path');
var express = require('express');
var cors = require('cors')
var app=express();
var fs = require('fs');
var sio = require('socket.io');



const port = process.env.PORT || 8080;

app.use(cors());
app.set("view engine","ejs");

var imageDir = path.join(__dirname, 'img');
var jsDir = path.join(__dirname, 'js_client_inc');
var libsDir = path.join(__dirname, 'js_client_inc/libs');

console.log(imageDir);
app.use("/img",express.static(imageDir));
app.use("/js_client_inc",express.static(jsDir));
app.use("/js_client_inc/libs",express.static(libsDir));


app.get("/",(req,res)=> {
  fs.readFile('./index.html', 'utf-8', function(error, content) {
    res.writeHead(200, {"Content-Type": "text/html"});
    res.end(content);
  });
});





var server = app.listen(port,function() {
	var host = server.address();
	// var port = server.address().port;
	console.log('listening on http://'+host);
});









var counter=0;

// Carichiamo il file index.html e mostriamo la pagina al visitatore

var ui={};
ui.positions={};


// Carichiamo Socket.io
var io = sio.listen(server);
// io.set('origins', '*:*');
// io.origins('*:*');
io.origins((origin, callback) => {
  // if (origin !== 'https://my.domain.com') {
  //   return callback('origin not allowed', false);
  // }
  callback(null, true);
});

console.log('Warming up');
  // console.log(io);



var sendTimeout=null;

// Quando i client si connettono, lo scriviamo nella console
io.sockets.on('connection', (socket) => {
	console.log('New user');
	// console.log(socket);
	counter++;

	socket.myId=counter;
	
	var allSockets=this;
  
/*
	this.socket.emit('connect', {"room":this.room,"id":"","pwd":""});
	this.socket.on('connect', function(content) {
		this.id=content.id;
		this.room=content.room;
		this.name=content.name;
	});

*/

	socket.on('firstconnect', function (data) {
		console.log('Access room:'+data.room+", by user "+this.myId);

		// Here test for data.id and data.pwd for logged users

		this.room=data.room;
		this.emit('firstconnect', {"room":data.room,"id":this.myId});

	}); 

	socket.on('message', function (message) {
		console.log('Un visitatore ha suonato il campanello! Dice:' + message.content);
	}); 
  
  
	socket.emit('message', { content:"Sei connesso! ("+counter+")", id: counter });
	
	socket.on('message', function (message) {
		console.log('Un visitatore ha suonato il campanello! Dice:' + message.content);
	}); 
	socket.on('positions', function (message) {
		// console.log(message.positions);
		//console.log(this.myId);
      	
		if(!ui.positions.hasOwnProperty(this.room)) ui.positions[this.room]={};
      	ui.positions[this.room][this.myId]={};
        
       ui.positions[this.room][this.myId]["timestamp"]=utils.getCurrentTimestamp();
        
		for(var it in message.positions) if(message.positions.hasOwnProperty(it)) {
			if(message.positions[it].x>0) ui.positions[this.room][this.myId][it]=message.positions[it];
		}
		
		// sendPositions();
		
		if(sendTimeout!=null) clearTimeout(sendTimeout);
		this.sendTimeout=setTimeout(sendPositions.bind(this),5);
	});
      
});

// setInterval(sendPositions,1000);




function sendPositions() {
	this.sendTimeout=null;
	// console.log(ui.positions[this.room]);
	var out={};
	
	for(var userId in ui.positions[this.room]) if(ui.positions[this.room].hasOwnProperty(userId)) {
		var cnt=0;
		if(utils.getCurrentTimestamp() - ui.positions[this.room][userId].timestamp > 500) {
			delete ui.positions[this.room][userId];
			continue;
		}
		for(var kind in ui.positions[this.room][userId]) if(ui.positions[this.room][userId].hasOwnProperty(kind)) {
			if(kind=="timestamp") continue;
			var src=ui.positions[this.room][userId][kind];
			
			out[userId+"_"+(cnt++)]={"x":src.x,"y":src.y,"t":src.t};
		}
	}
	//console.log(out);
	//console.log("\n");
	
	io.sockets.emit('data-update', {"positions":out});
}


var utils={};

utils.getCurrentTimestamp=function() {
	return Date.now(); // not IE8 (node ok)
}
